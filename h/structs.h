#ifndef STRUCTS_H
#define STRUCTS_H

typedef struct Point Point;

struct Point
{
	double dim_1;
	double dim_2;
};

struct LabeledCluster
{
	Point * data_pts;
	int dim;
	int size;
};

struct Separation
{
	double gamma;
	double tau;
	Point theta;
	Point * beta;
};

struct Bin
{
	double ceiling;
	double floor;
	int count;
};

struct ErrorThres
{
	double Jt;
	double Jt_pr;
	int thres;
	double mean1;
	double mean2;
	double sigma1;
	double sigma2;
};

#endif
